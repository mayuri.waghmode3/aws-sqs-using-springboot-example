package com.max.controller;

import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.SendMessageBatchResult;
import com.max.service.QueueOperationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class controller {
    @Autowired
    QueueOperationsService queueOperationsService;
    @GetMapping("/create/std")
    public String createStdQ(String qNm)  {
        return queueOperationsService.createStandardQueue(qNm);
    }
    @GetMapping("/create/fifo")
    public String createFiFoQueue(String qNm) {
        return queueOperationsService.createFiFoQueue(qNm);
    }
    @PutMapping("/send/ToStdQ")
    public String postMsgToStdQ(String msg) {
        return queueOperationsService.PostMsgToStdQ(msg);
    }
    @PutMapping("/send/ToFifiQ")
    public String PostMsgToFifioQ(String msg) {
        return queueOperationsService.PostMsgToFifioQ(msg);
    }
    @GetMapping("/read/msgFromQ")
    public List<Message> readingMsgFromQ(String url) {
            return queueOperationsService.readingMsgFromQ(url);
    }
    @GetMapping("/send/batchToFifo")
    public SendMessageBatchResult batchToFifo(){
        return queueOperationsService.PostMultipleMsgToQ();
    }
    @GetMapping("/delete/message")
    public String deleteMessage(){
        return queueOperationsService.deleteingMsgfrmQ();
    }
    @GetMapping("/monitoring")
    public String monitoring(){
        return queueOperationsService.monitoring();
    }

    @GetMapping("/deadLetterQ")
    public String deadLetterQ(){
        return queueOperationsService.deadLetterQ();
    }
}
