package com.max.serviceImpl;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.*;
import com.max.service.QueueOperationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
@ComponentScan
public class QueueOperationsImpl implements QueueOperationsService {
    @Autowired
    AmazonSQS sqs;

    @Value("${amazon.sqs.fifo}")
    String fifoUrl;

    @Value("${amazon.sqs.std}")
    String stdUrl;

    public String createStandardQueue(String qNm)  {
        //standard queue

        String standardQueueUrl =  sqs.createQueue(qNm).getQueueUrl();
        return standardQueueUrl;
    }
    public String createFiFoQueue(String qNm) {
        //Fifo queue
        Map<String,String> queueAttributes = new HashMap<>();
        queueAttributes.put("FifoQueue","true");
        queueAttributes.put("ContentBasedDeduplication","true");
        CreateQueueRequest createQueueRequest =
                new CreateQueueRequest(qNm).withAttributes(queueAttributes);
        String QueueUrl =  sqs.createQueue(createQueueRequest).getQueueUrl();
        return QueueUrl;
    }


    @Override
    public String PostMsgToStdQ(String msg) {
       SendMessageRequest sendMessageRequest = new SendMessageRequest()
                .withQueueUrl(stdUrl)
                .withMessageBody("Hi sending message to std q."+msg)
                .withDelaySeconds(30);
      sqs.sendMessage(sendMessageRequest);
      return "message sent!";
    }

    @Override
    public String PostMsgToFifioQ(String msg)  {

        final SendMessageRequest sendMessageRequest= new SendMessageRequest(fifoUrl,msg);
        sendMessageRequest.setMessageGroupId("messageGroup1");
        Map<String, MessageAttributeValue> messageAttributes = new HashMap<>();
        messageAttributes.put("AttributeOne", new MessageAttributeValue()
                .withStringValue("This is an attribute")
                .withDataType("String"));
        sendMessageRequest.setMessageAttributes(messageAttributes);
        final SendMessageResult sendMessageResult = sqs.sendMessage(sendMessageRequest);

        final String messageId = sendMessageResult.getMessageId();

        return messageId;
    }

    @Override
    public SendMessageBatchResult PostMultipleMsgToQ() {
        List <SendMessageBatchRequestEntry> messageEntries = new ArrayList<>();
        messageEntries.add(new SendMessageBatchRequestEntry()
                .withId("id-1")
                .withMessageBody("batch-1")
                .withMessageGroupId("group-1"));
        messageEntries.add(new SendMessageBatchRequestEntry()
                .withId("id-2")
                .withMessageBody("batch-2")
                .withMessageGroupId("group-1"));

        SendMessageBatchRequest sendMessageBatchRequest
                = new SendMessageBatchRequest(fifoUrl, messageEntries);
        SendMessageBatchResult result = sqs.sendMessageBatch(sendMessageBatchRequest);
        return result;
    }

    @Override
    public  List<Message> readingMsgFromQ(String url)  {
        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(url)
                .withWaitTimeSeconds(10)
                .withMaxNumberOfMessages(10);
        List<Message> sqsMessages = null;

        sqsMessages = sqs.receiveMessage(receiveMessageRequest).getMessages();

        return sqsMessages;
    }

    @Override
    public String deleteingMsgfrmQ() {
        List<Message> sqsMessages = this.readingMsgFromQ(fifoUrl);
        DeleteMessageResult result = sqs.deleteMessage(new DeleteMessageRequest().withQueueUrl(fifoUrl)
                .withReceiptHandle(sqsMessages.get(0).getReceiptHandle()));
        System.out.println(sqsMessages.get(0));
        return result.toString();
    }

    @Override
    public String deadLetterQ() {
        String deadLetterQueueUrl = sqs.createQueue("dead-letter-queue")
                .getQueueUrl();
        GetQueueAttributesResult queueAttributesResult = sqs.getQueueAttributes(new GetQueueAttributesRequest(deadLetterQueueUrl).withAttributeNames("QueueArn"));
        String deadQArn = queueAttributesResult.getAttributes().get("QueueArn");
        SetQueueAttributesRequest setQueueAttributesRequest = new SetQueueAttributesRequest()
                .withQueueUrl(stdUrl)
                .addAttributesEntry("RedrivePolicy", "{\"maxReceiveCount\":\"2\", " + "\"deadLetterTargetArn\":\"" + deadQArn + "\"}");


        sqs.setQueueAttributes(setQueueAttributesRequest);

        return "Dead queue created for " + stdUrl ;
    }

    @Override
    public String monitoring() {
        GetQueueAttributesRequest getQueueAttributesRequest = new GetQueueAttributesRequest(stdUrl).withAttributeNames("All");
        GetQueueAttributesResult getQueueAttributesResult =sqs.getQueueAttributes(getQueueAttributesRequest);
        String output = "The number of messages on the queue: "+ getQueueAttributesResult.getAttributes().get("ApproximateNumberOfMessages")+"\n"
                +"The number of messages in flight: "+ getQueueAttributesResult.getAttributes().get("ApproximateNumberOfMessagesNotVisible");

        return output;
    }

}
