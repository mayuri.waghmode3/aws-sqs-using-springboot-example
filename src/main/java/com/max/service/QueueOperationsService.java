package com.max.service;

import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.SendMessageBatchResult;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface QueueOperationsService {
    String createStandardQueue(String qNm) ;
    String createFiFoQueue(String qNm) ;
    String PostMsgToStdQ(String msg);
    String PostMsgToFifioQ(String msg);
    SendMessageBatchResult PostMultipleMsgToQ();
    List<Message> readingMsgFromQ(String url);
    String deleteingMsgfrmQ();
    String deadLetterQ();
    String monitoring();
}
